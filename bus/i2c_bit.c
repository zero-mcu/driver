#include <bus/i2c_bit.h>


#define SCL_H(i2c_bit)              gpio_write(i2c_bit->scl, GPIO_LEVEL_HIGH)
#define SCL_L(i2c_bit)              gpio_write(i2c_bit->scl, GPIO_LEVEL_LOW)
#define SDA_H(i2c_bit)              gpio_write(i2c_bit->sda, GPIO_LEVEL_HIGH)
#define SDA_L(i2c_bit)              gpio_write(i2c_bit->sda, GPIO_LEVEL_LOW)
#define SDA_STATE(i2c_bit)          (GPIO_LEVEL_HIGH == gpio_read(i2c_bit->sda))


int i2c_bit_start(i2c_bit_t* i2c_bit)
{
    gpio_write_dir(i2c_bit->sda, GPIO_DIR_OUTPUT);
    SDA_H(i2c_bit);
    SCL_H(i2c_bit);
    delay_us(i2c_bit->delay_us);
    SDA_L(i2c_bit);
    delay_us(i2c_bit->delay_us);
    SCL_L(i2c_bit);
    delay_us(i2c_bit->delay_us);
    return 0;
}

int i2c_bit_stop(i2c_bit_t* i2c_bit)
{
    gpio_write_dir(i2c_bit->sda, GPIO_DIR_OUTPUT);
    SDA_L(i2c_bit);
    delay_us(i2c_bit->delay_us);
    SCL_H(i2c_bit);
    delay_us(i2c_bit->delay_us);
    SDA_H(i2c_bit);
    delay_us(i2c_bit->delay_us);
    return 0;
}

int i2c_bit_writeb(i2c_bit_t* i2c_bit, ze_u8_t data, i2c_ack_t *ack)
{
    ze_u8_t temp = 0;
    gpio_write_dir(i2c_bit->sda, GPIO_DIR_OUTPUT);
    for(int i = 0;i < 8;i++)
    {
        temp = data&0x80;
        data <<= 1;
        if(temp)
            SDA_H(i2c_bit);
        else
            SDA_L(i2c_bit);
        delay_us(i2c_bit->delay_us);
        SCL_H(i2c_bit);
        delay_us(i2c_bit->delay_us);
        SCL_L(i2c_bit);
        delay_us(i2c_bit->delay_us);
    }
    SDA_H(i2c_bit);
    gpio_write_dir(i2c_bit->sda, GPIO_DIR_INPUT);
    delay_us(i2c_bit->delay_us);
    SCL_H(i2c_bit);
    delay_us(i2c_bit->delay_us);
    if(SDA_STATE(i2c_bit))
        *ack = I2C_NACK;
    else
        *ack = I2C_ACK;

    SCL_L(i2c_bit);
    delay_us(i2c_bit->delay_us);

    return 0;
}
int i2c_bit_readb(i2c_bit_t* i2c_bit, ze_u8_t *data, i2c_ack_t ack)
{
    int temp = 0;

    SDA_H(i2c_bit);

    gpio_write_dir(i2c_bit->sda, GPIO_DIR_INPUT);

    delay_us(i2c_bit->delay_us);
    for(int i=0;i<8;i++)
    {
        temp <<= 1;
        SCL_H(i2c_bit);
        delay_us(i2c_bit->delay_us);
        if(SDA_STATE(i2c_bit))
            temp |= 0x01;
        SCL_L(i2c_bit);
        delay_us(i2c_bit->delay_us);
    }

    gpio_write_dir(i2c_bit->sda, GPIO_DIR_OUTPUT);

    if(ack == I2C_ACK)
        SDA_L(i2c_bit);
    else
        SDA_H(i2c_bit);
    SCL_H(i2c_bit);
    delay_us(i2c_bit->delay_us);
    SCL_L(i2c_bit);
    delay_us(i2c_bit->delay_us);
    *data = temp;

    return 0;
}


int i2c_bit_xfer(i2c_t* i2c, i2c_dev_addr_t dev_addr, i2c_msg_t* msgs, ze_size_t nmsg)
{
    int ret = 0;
    i2c_ack_t ack;
    ze_u8_t* data;
    i2c_bit_t* i2c_bit = CONTAINER_OF(i2c, i2c_bit_t, i2c);

    for (int i = 0; i < nmsg; i++)
    {
        i2c_msg_t* msg = msgs + i;
        data = msg->data;
        if (msg->flag == I2C_WRITE)
        {
            if ((ret = i2c_bit_start(i2c_bit)) != 0)
                goto i2c_error;
            if(i2c_bit_writeb(i2c_bit, (I2C_ADDR(dev_addr) << 1) | 0x00, &ack)<0 || ack != I2C_ACK)
                goto i2c_error;
            for(int j = 0; j < msg->len; j++)
            {
                if(i2c_bit_writeb(i2c_bit, *data ++, &ack) < 0 || ack != I2C_ACK)
                    goto i2c_error;
            }
        }
        else if (msg->flag == I2C_READ)
        {
            if(i2c_bit_start(i2c_bit) < 0)
                goto i2c_error;
            if(i2c_bit_writeb(i2c_bit, (I2C_ADDR(dev_addr) << 1) | 0x01, &ack)<0 || ack != I2C_ACK)
                goto i2c_error;
            for(int j = 0; j < msg->len; j++)
            {
                if(j == msg->len-1)
                {
                    if(i2c_bit_readb(i2c_bit, data ++, I2C_NACK)<0)
                        goto i2c_error;
                }
                else
                {
                    if(i2c_bit_readb(i2c_bit, data ++, I2C_ACK)<0)
                        goto i2c_error;
                }
            }
        }
        else if (msg->flag == I2C_APPEND)
        {
            for (int j = 0; j < msg->len; j++)
            {
                if (i2c_bit_writeb(i2c_bit, *data++, &ack) || ack != I2C_ACK)
                    goto i2c_error;

            }
        }
    }

    i2c_bit_stop(i2c_bit);

    return 0;
i2c_error:
    i2c_bit_stop(i2c_bit);
    return ENO_ERROR;
}

const i2c_algorithm_t i2c_bit_algo = {
    .xfer = i2c_bit_xfer,
};

i2c_t* i2c_bit_init(i2c_bit_t* i2c_bit, gpio_t* scl, gpio_t* sda, ze_u32_t speed_hz)
{
    ASSERT(i2c_bit);
    ASSERT(scl);
    ASSERT(sda);
    i2c_bit->scl = scl;
    i2c_bit->sda = sda;

    i2c_init(&i2c_bit->i2c, &i2c_bit_algo);
    i2c_bit->delay_us = 1000000 / speed_hz / 2;

    return &i2c_bit->i2c;
}