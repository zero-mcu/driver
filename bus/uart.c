#include <bus/uart.h>


void uart_init(uart_t* uart, const uart_ops_t* ops, ze_u8_t flags)
{
    ASSERT(uart);
    ASSERT(ops);
    uart->ops = ops;
    uart->flags = flags;
}

static int _uart_poll_write(uart_t *uart, const ze_u8_t* wr_data, ze_size_t wr_len)
{
    const uart_ops_t *ops = uart->ops;

    int length = wr_len;

    while(length)
    {
        if(ops->put_char(uart, *wr_data ++) < 0)
            break;
        length --;
    }

    return wr_len - length;
}

static int _uart_poll_read(uart_t *uart, ze_u8_t *rd_data, ze_size_t rd_len)
{
    const uart_ops_t *ops = uart->ops;

    int ch = -1;
    int length = rd_len;

    while(length)
    {
        ch = ops->get_char(uart);
        if(ch < 0)
            break;
        *rd_data ++  = ch;
        length --;
    }
    return rd_len - length;
}

static int _uart_irq_write(uart_t *uart, const ze_u8_t* wr_data, ze_size_t wr_len)
{
    ASSERT(uart->uart_tx);

    uart_fifo_t* tx_fifo = (uart_fifo_t*)uart->uart_tx;
    int length = wr_len;

    while(length)
    {
        if(tx_fifo->put_index != tx_fifo->get_index)
        {
            tx_fifo->buffer[tx_fifo->put_index] = *wr_data ++;
            tx_fifo->put_index ++;
            if(tx_fifo->put_index >= tx_fifo->bufsz)
                tx_fifo->put_index = 0;
        }
        length --;
    }
    return wr_len - length;
}

static int _uart_irq_read(uart_t *uart, ze_u8_t *rd_data, ze_size_t rd_len)
{
    ASSERT(uart->uart_rx);

    uart_fifo_t *rx_fifo = uart->uart_rx;
    int length = rd_len;
    while(length)
    {
        int ch;
        if(rx_fifo->get_index != rx_fifo->put_index)
        {
            ch = rx_fifo->buffer[rx_fifo->get_index];
            rx_fifo->get_index ++;
            if(rx_fifo->get_index >= rx_fifo->bufsz)
                rx_fifo->get_index = 0;
        }
        else
            break;
        *rd_data ++ = ch & 0xFF;
        length --;
    }
    return rd_len - length;
}

static int _uart_dma_write(uart_t *uart, const ze_u8_t* wr_data, ze_size_t wr_len)
{
    uart_dma_t* dma = uart->uart_tx;
    if (dma->transmit)
        return dma->transmit(uart, (ze_u8_t*)wr_data, wr_len);
    return 0;
}

static int _uart_dma_read(uart_t *uart, ze_u8_t *rd_data, ze_size_t rd_len)
{
    uart_dma_t* dma = uart->uart_rx;
    if (dma->transmit)
        return dma->transmit(uart, rd_data, rd_len);
    return 0;
}


int uart_write(uart_t* uart, const ze_u8_t* wr_data, ze_size_t wr_len)
{
    ASSERT(uart);
    ASSERT(uart->ops);
    ASSERT(wr_data);
    ASSERT(wr_len > 0);

    if(uart->flags & UART_FLAG_IRQ_TX)
        return _uart_irq_write(uart, wr_data, wr_len);
    else if(uart->flags & UART_FLAG_DMA_TX)
        return _uart_dma_write(uart, wr_data, wr_len);
    else
        return _uart_poll_write(uart, wr_data, wr_len);
}

int uart_read(uart_t* uart, ze_u8_t* rd_data, ze_size_t rd_len)
{
    ASSERT(uart);
    ASSERT(uart->ops);
    ASSERT(rd_data);
    ASSERT(rd_len > 0);

    if(uart->flags & UART_FLAG_IRQ_RX)
        return _uart_irq_read(uart, rd_data, rd_len);
    else if(uart->flags & UART_FLAG_DMA_RX)
        return _uart_dma_read(uart, rd_data, rd_len);
    else
        return _uart_poll_read(uart, rd_data, rd_len);
}

void uart_set_notify(uart_t* uart, void (*rx_notify)(void), void (*tx_notify)(void))
{
    ASSERT(uart);
    ASSERT(rx_notify);
    ASSERT(tx_notify);
    uart->rx_notify = rx_notify;
    uart->tx_notify = tx_notify;
}

void uart_rx_notify(uart_t* uart)
{
    ASSERT(uart);
    if (uart->rx_notify)
        uart->rx_notify();
}

void uart_tx_notify(uart_t* uart)
{
    ASSERT(uart);
    if (uart->tx_notify)
        uart->tx_notify();
}

void uart_hw_isr(uart_t* uart, ze_u8_t event)
{
    ASSERT(uart);
    ASSERT(uart->ops);

    const uart_ops_t *ops = uart->ops;
    switch(event & 0xFF)
    {
        case UART_EVENT_IRQ_RX:
        {
            int ch = -1;
            uart_fifo_t *rx_fifo = (uart_fifo_t*)uart->uart_rx;
            while(1)
            {
                ch = ops->get_char(uart);
                if(ch == -1)
                    break;
                rx_fifo->buffer[rx_fifo->put_index]=ch;
                rx_fifo->put_index += 1;
                if(rx_fifo->put_index>=rx_fifo->bufsz)
                    rx_fifo->put_index = 0;
                if(rx_fifo->put_index == rx_fifo->get_index)
                {
                    rx_fifo->get_index += 1;
                    if(rx_fifo->put_index >= rx_fifo->bufsz)
                    {
                        rx_fifo->get_index = 0;
                    }
                }
            }
            break;
        }
        case UART_EVENT_IRQ_TX:
        {
            int ch;
            uart_fifo_t *tx_fifo = (uart_fifo_t*)uart->uart_tx;
            if(tx_fifo->get_index!=tx_fifo->put_index)
            {
                ch = tx_fifo->buffer[tx_fifo->get_index];
                if(ops->put_char(uart,ch)<0)
                    break;
                tx_fifo->get_index +=1;
                if(tx_fifo->get_index>=tx_fifo->bufsz)
                    tx_fifo->get_index = 0;
            }
            break;
        }
        case UART_EVENT_DMA_RX:
        {
            uart_rx_notify(uart);
            break;
        }
        case UART_EVENT_DMA_TX:
        {
            uart_tx_notify(uart);
            break;
        }
    }
}
