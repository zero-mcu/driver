#include <chip/gpio.h>


void gpiochip_init(gpiochip_t* chip, ze_u16_t port_pins, const gpiochip_ops_t* ops)
{
    ASSERT(chip);
    ASSERT(ops);
    chip->port_pins = port_pins;
    chip->ops = ops;
}

int gpiochip_write(gpiochip_t* chip, ze_u8_t port, ze_u16_t value)
{
    ASSERT(chip);
    if (chip->ops->write_output)
        return chip->ops->write_output(chip, port, value & ((1 << chip->port_pins) - 1));
    return ENO_INVAL;
}

int gpiochip_read(gpiochip_t* chip, ze_u8_t port)
{
    ASSERT(chip);
    if (chip->ops->read_input)
        return chip->ops->read_input(chip, port);
    return ENO_INVAL;
}

int gpiochip_read_output(gpiochip_t* chip, ze_u8_t port)
{
    ASSERT(chip);
    if (chip->ops->read_output)
        return chip->ops->read_output(chip, port);
    return ENO_INVAL;
}

int gpiochip_write_dir(gpiochip_t* chip, ze_u8_t port, ze_u16_t value)
{
    ASSERT(chip);
    if (chip->ops->write_dir)
        return chip->ops->write_dir(chip, port, value & ((1 << chip->port_pins) - 1));
    return ENO_INVAL;
}

int gpiochip_read_dir(gpiochip_t* chip, ze_u8_t port)
{
    ASSERT(chip);
    if (chip->ops->read_dir)
        return chip->ops->read_dir(chip, port);
    return ENO_INVAL;
}

int gpiochip_write_pin_dir(gpiochip_t* chip, ze_u8_t pin, gpio_dir_t dir)
{
    ASSERT(chip);
    if (chip->ops->write_pin_dir)
        return chip->ops->write_pin_dir(chip, pin, dir);

    ze_u8_t port = pin / chip->port_pins + (pin % chip->port_pins ? 1 : 0);
    pin = pin % chip->port_pins;
    int value = 0;
    if ((value = gpiochip_read_dir(chip, port)) < 0)
        return value;
    value = value & ~(1 << pin);
    value |= (dir << pin);
    return gpiochip_write_dir(chip, port, value);
}

int gpiochip_read_pin_dir(gpiochip_t* chip, ze_u8_t pin)
{
    ASSERT(chip);
    if (chip->ops->read_pin_dir)
        return chip->ops->read_pin_dir(chip, pin);

    ze_u8_t port = pin / chip->port_pins + (pin % chip->port_pins ? 1 : 0);
    pin = pin % chip->port_pins;
    int value = 0;
    if ((value = gpiochip_read_dir(chip, port)) < 0)
        return value;
    return (value >> pin) & 0x01;
}

int gpiochip_write_pin(gpiochip_t* chip, ze_u8_t pin, gpio_level_t level)
{
    if (chip->ops->write_pin)
        return chip->ops->write_pin(chip, pin, level);

    ze_u8_t port = pin / chip->port_pins + (pin % chip->port_pins ? 1 : 0);
    pin = pin % chip->port_pins;
    int value = 0;
    if ((value = gpiochip_read_output(chip, port)) < 0)
        return value;
    value = value & ~(1 << pin);
    value |= (level << pin);
    return gpiochip_write_dir(chip, port, value);
}

int gpiochip_read_pin(gpiochip_t* chip, ze_u8_t pin)
{
    ASSERT(chip);
    if (chip->ops->read_pin)
        return chip->ops->read_pin(chip, pin);

    ze_u8_t port = pin / chip->port_pins + (pin % chip->port_pins ? 1 : 0);
    pin = pin % chip->port_pins;
    int value = 0;
    if ((value = gpiochip_read(chip, port)) < 0)
        return value;

    return (value >> pin) & 0x01;
}

void gpio_init(gpio_t* gpio, gpiochip_t* chip, ze_u16_t pin)
{
    ASSERT(gpio);
    ASSERT(chip);
    gpio->chip = chip;
    gpio->pin = pin;
}

int gpio_write(gpio_t* gpio, gpio_level_t level)
{
    ASSERT(gpio);
    return gpiochip_write_pin(gpio->chip, gpio->pin, level);
}

int gpio_read(gpio_t* gpio)
{
    ASSERT(gpio);
    return gpiochip_read_pin(gpio->chip, gpio->pin);
}

int gpio_write_dir(gpio_t* gpio, gpio_dir_t dir)
{
    ASSERT(gpio);
    return gpiochip_write_pin_dir(gpio->chip, gpio->pin, dir);
}

int gpio_read_dir(gpio_t* gpio)
{
    ASSERT(gpio);
    return gpiochip_read_pin_dir(gpio->chip, gpio->pin);
}
