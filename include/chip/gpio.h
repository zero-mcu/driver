/**
 * @file gpio.h
 * @brief This is the difinition of gpio.
 * @author yuanle
 * @version 0.1
 * @details Change history:
 * <Data>        |<Version>    |<Author>        |<Description>
 * -----------------------------------------------------------
 * 2019/06/10    |0.1          |yuanle          |Initial Version
 * -----------------------------------------------------------
 * @date 2019-06-10
 * @license Dual BSD/GPL
 */
#ifndef __CHIP_GPIO_H__
#define __CHIP_GPIO_H__
#include <system.h>

#ifdef __cplusplus
extern "C"{
#endif

typedef enum {
    GPIO_LEVEL_LOW = 0,
    GPIO_LEVEL_HIGH
} gpio_level_t;

typedef enum {
    GPIO_DIR_OUTPUT = 0,
    GPIO_DIR_INPUT,
} gpio_dir_t;

typedef struct {
    ze_u16_t port_pins;
    const struct gpiochip_operation* ops;
} gpiochip_t;

typedef struct gpiochip_operation {
    int (*write_output)(gpiochip_t* chip, ze_u8_t port, ze_u16_t value);
    int (*read_input)(gpiochip_t* chip, ze_u8_t port);
    int (*read_output)(gpiochip_t* chip, ze_u8_t port);
    int (*write_dir)(gpiochip_t* chip, ze_u8_t port, ze_u16_t value);
    int (*read_dir)(gpiochip_t* chip, ze_u8_t port);
    int (*write_pin)(gpiochip_t* chip, ze_u8_t pin, gpio_level_t level);
    int (*read_pin)(gpiochip_t* chip, ze_u8_t pin);
    int (*write_pin_dir)(gpiochip_t* chip, ze_u8_t pin, gpio_dir_t dir);
    int (*read_pin_dir)(gpiochip_t* chip, ze_u8_t pin);
} gpiochip_ops_t;

typedef struct {
    gpiochip_t* chip;
    ze_u8_t port;
    ze_u16_t pin;
} gpio_t;

void gpiochip_init(gpiochip_t* chip, ze_u16_t port_pins, const gpiochip_ops_t* ops);

int gpiochip_write(gpiochip_t* chip, ze_u8_t port, ze_u16_t value);

int gpiochip_read(gpiochip_t* chip, ze_u8_t port);

int gpiochip_read_output(gpiochip_t* chip, ze_u8_t port);

int gpiochip_write_dir(gpiochip_t* chip, ze_u8_t port, ze_u16_t value);

int gpiochip_read_dir(gpiochip_t* chip, ze_u8_t port);

int gpiochip_write_pin_dir(gpiochip_t* chip, ze_u8_t pin, gpio_dir_t dir);

int gpiochip_read_pin_dir(gpiochip_t* chip, ze_u8_t pin);

int gpiochip_write_pin(gpiochip_t* chip, ze_u8_t pin, gpio_level_t level);

int gpiochip_read_pin(gpiochip_t* chip, ze_u8_t pin);

void gpio_init(gpio_t* gpio, gpiochip_t* chip, ze_u16_t pin);

int gpio_write(gpio_t* gpio, gpio_level_t level);

int gpio_read(gpio_t* gpio);

int gpio_write_dir(gpio_t* gpio, gpio_dir_t dir);

int gpio_read_dir(gpio_t* gpio);

#ifdef __cplusplus
}
#endif
#endif