/**
 * @file i2c_bit.h
 * @brief This is the difinition of i2c bit algorithm.
 * @author yuanle
 * @version 0.1
 * @details Change history:
 * <Data>        |<Version>    |<Author>        |<Description>
 * -----------------------------------------------------------
 * 2019/06/16    |0.1          |yuanle          |Initial Version
 * -----------------------------------------------------------
 * @date 2019-06-16
 * @license Dual BSD/GPL
 */
#ifndef __I2C_BIT_H__
#define __I2C_BIT_H__

#include <bus/i2c.h>
#include <chip/gpio.h>


#ifdef __cplusplus
extern "C"{
#endif

typedef struct {
	ze_usec_t delay_us;
	i2c_t i2c;
	gpio_t* scl;
	gpio_t* sda;
} i2c_bit_t;

i2c_t* i2c_bit_init(i2c_bit_t* i2c_bit, gpio_t* scl, gpio_t* sda, ze_u32_t speed_hz);

#ifdef __cplusplus
}
#endif
#endif
