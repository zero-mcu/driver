/**
 * @file uart.h
 * @brief This is the difinition of uart.
 * @author yuanle
 * @version 0.1
 * @details Change history:
 * <Data>        |<Version>    |<Author>        |<Description>
 * -----------------------------------------------------------
 * 2019/10/08    |0.1          |yuanle          |Initial Version
 * -----------------------------------------------------------
 * @date 2019-10-08
 * @license Dual BSD/GPL
 */
#ifndef __BUS_UART_H__
#define __BUS_UART_H__
#include <system.h>

#ifdef __cplusplus
extern "C"{
#endif


#define UART_FLAG_IRQ_TX        0x01
#define UART_FLAG_DMA_TX        0x02

#define UART_FLAG_IRQ_RX        0x10
#define UART_FLAG_DMA_RX        0x20


#define UART_EVENT_IRQ_TX       0
#define UART_EVENT_IRQ_RX       1
#define UART_EVENT_DMA_TX       2
#define UART_EVENT_DMA_RX       3

typedef struct uart_fifo {
    ze_u8_t* buffer;
    int put_index, get_index;
    int bufsz;
} uart_fifo_t;

typedef struct uart{
    ze_u8_t flags;
    void* uart_rx;
    void* uart_tx;
    void* private_data;
    const struct uart_operation* ops;
    void (*rx_notify)(void);
    void (*tx_notify)(void);
} uart_t;

typedef struct uart_operation {
    int (*put_char)(struct uart* uart, int ch);
    int (*get_char)(struct uart* uart);
} uart_ops_t;

typedef struct uart_dma {
    int (*transmit)(struct uart* uart, ze_u8_t* data, ze_size_t len);
} uart_dma_t;

void uart_init(uart_t* uart, const uart_ops_t* ops, ze_u8_t flags);

int uart_write(uart_t* uart, const ze_u8_t* wr_data, ze_size_t wr_len);

int uart_read(uart_t* uart, ze_u8_t* rd_data, ze_size_t rd_len);

void uart_set_notify(uart_t* uart, void (*rx_notify)(void), void (*tx_notify)(void));

void uart_rx_notify(uart_t* uart);

void uart_tx_notify(uart_t* uart);

void uart_hw_isr(uart_t* uart, ze_u8_t event);

#ifdef __cplusplus
}
#endif
#endif // __BUS_UART_H__