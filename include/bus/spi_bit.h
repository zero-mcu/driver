/**
 * @file spi_bit.h
 * @brief This is the difinition of spi_bit.
 * @author yuanle
 * @version 0.1
 * @details Change history:
 * <Data>        |<Version>    |<Author>        |<Description>
 * -----------------------------------------------------------
 * 2019/06/16    |0.1          |yuanle          |Initial Version
 * -----------------------------------------------------------
 * @date 2019-06-16
 * @license Dual BSD/GPL
 */
#ifndef __SPI_BIT_H__
#define __SPI_BIT_H__

#ifdef __cplusplus
extern "C"{
#endif

#include <bus/spi.h>
#include <chip/gpio.h>

typedef struct {
    spi_t spi;
    gpio_t* miso;
    gpio_t* mosi;
    gpio_t* sck;
    gpio_t* cs;
    ze_u16_t delay_us;
    spi_mode_t mode;
} spi_bit_t;

spi_t* spi_bit_init(spi_bit_t* spi_bit, gpio_t* miso, gpio_t* mosi, gpio_t* sck, gpio_t* cs);

#ifdef __cplusplus
}
#endif
#endif